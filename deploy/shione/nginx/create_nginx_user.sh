#!/bin/sh

set -eu

adduser --system --no-create-home --verbose --debug nginx

apt install nginx certbot python3-certbot-nginx

certbot --nginx -d shione.net -d www.shione.net
