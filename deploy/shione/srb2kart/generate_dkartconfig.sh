#!/bin/sh

set -eux

# Generates the portion of `dconfig.cfg` loading all necessary mod files in the
# correct order. 

# The script assumes a hierarchy demonstrated by the following example.
# mods/
#     000-pre/
#     001-chars/
#     002-tracks/
#     003-post/

# This allows separation of mods based on category such as characters and
# soundtracks but also serves as a poor man's dependency resolution between
# groups of mods e.g., gameplay mods depending on specific characters. The same
# approach can be used to solve dependency between mods found in the same
# category, prefixing the mods with a sequence of digits *should* solve the
# dependency problem e.g., `000-init-mod`, `001-mod-depending-on-000` and
# `002-mod-depending-on-001`.

# While it shouldn't be hard to support a depth of more than 1 subdirectory, it
# seems impractical to me.

if [ -e dkartconfig.cfg ]; then
  rm -- dkartconfig.cfg
fi

if [ -e dkartconfig_generated.cfg ]; then
  rm -- dkartconfig_generated.cfg
fi

cp -- dkartconfig_base.cfg dkartconfig.cfg

# NOTE: `dkartconfig_password.cfg` *must* follow the format `password xxx`.
# TODO: Read from an environment variable?
cat -- dkartconfig_password.cfg >>dkartconfig.cfg

# NOTE: `find` does not offer a way to guarantee a desired sort of its output.
find mods -type f -printf 'addfile %p\n' | sort >dkartconfig_generated.cfg

cat -- dkartconfig_generated.cfg >>dkartconfig.cfg
rm -- dkartconfig_generated.cfg

if [ -e mods/index ]; then
	rm -rf -- mods/index
fi

printf 'regenerating index...\n'
mkdir -- mods/index
cd -- mods/index

# NOTE: Mods from different categories having the same name are not supported.
# XXX: Please don't use newlines in filenames for the love of god.
find .. -type f -print | while read -r file; do
	ln -s "$file" .
done

cd -- -
