#!/bin/sh

set -eux

# Install both build and runtime dependencies. 
# 
# Build dependencies are install just in case srb2kart needs to be compiled on
# shione.
apt install \
  make \
  git \
  nasm \
  gcc \
  libsdl2-mixer-dev \
  libpng-dev \
  libcurl4-openssl-dev \
  libgme-dev \
  libopenmpt-dev \
  byobu

# The user `srb2kart` is used by default here.
su srb2kart
cd --
