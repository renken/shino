Quoting [this thread on srb2
forum](https://mb.srb2.org/threads/dedicated.7063/).

> Although that's the way it's often been used in practice, it's not actually
> the case: adedserv.cfg is the counterpart of autoexec.cfg; dconfig.cfg of
> config.cfg. This has important implications for setting the masterserver: it
> works in the latter, but by the time the former is executed, it's too late.


In this case, I'll put everything in `dconfig.cfg` and see later on if there
are things I can delay to `adedserv.cfg`. I think the mods need to be
broadcasted to the master server? I could be wrong here.
