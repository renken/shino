#!/bin/sh

set -eu

./generate_dkartconfig.sh

rsync \
  -a \
  --progress \
  srb2kart.socket \
  srb2kart.service \
  srb2kart@shione:/home/srb2kart/.config/systemd/user

rsync \
  -a \
  --delete \
  --progress \
  dkartconfig.cfg \
  mods \
  srb2kart@shione:/home/srb2kart/.srb2kart
