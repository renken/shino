# TODO

## Configuration

## Packaging
* Package a recent version of neovim for Debian.
* Port neovim config to (pure) lua.
* Figure out a way to quickly package neovim plugins instead of relying on a
  thirdparty plugin manager?
