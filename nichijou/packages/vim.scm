(define-module (nichijou packages vim)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system copy)
  #:use-module (gnu packages)
  #:use-module (gnu packages vim)
  #:use-module (gnu packages uml))

(define-public vim-plantuml-syntax
  (package
    (name "vim-plantuml-syntax")
    (version "0.2.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/aklt/plantuml-syntax")
                    (commit "660bbb1ece1e654b2176f76ce0689304c5a4a025")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "05xf36np8y0gdcd05nzmkawh131lp4mm5cjgjr1byr8cjyl1idr3"))))
    (build-system copy-build-system)
    (arguments
     '(#:install-plan '(("ftdetect" "share/vim/vimfiles/")
                        ("ftplugin" "share/vim/vimfiles/")
                        ("indent" "share/vim/vimfiles/")
                        ("syntax" "share/vim/vimfiles/"))))
    (synopsis "Vim PlantUML Syntax/Plugin/FTDetect")
    (inputs (list plantuml))
    (description
     "This is a vim syntax file for PlantUML.The filetype will be set to
plantuml for @code{*.pu}, @code{*.uml}, @code{*.puml}, @code{*.iuml} or
@code{*.plantuml} files or if the first line of a file contains
@code{@@startuml}. Additionally the makeprg is set to plantuml assuming you
have this executable in your path.")
    (home-page "https://github.com/aklt/plantuml-syntax")
    (license #f)))

(define-public vim-doxygen-toolkit
  (package
    (name "vim-doxygen-toolkit")
    (version "0.2.13")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/vim-scripts/DoxygenToolkit.vim")
                    (commit version)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1za8li02j4nhqjjsyxg4p78638h5af4izim37zc0p1x55zr3i85r"))))
    (build-system copy-build-system)
    (arguments
     '(#:install-plan '(("plugin" "share/vim/vimfiles/"))))
    (synopsis "Simplify Doxygen documentation in C, C++, Python.")
    (description
     "Currently five purposes have been defined :
@itemize
@item Generates a doxygen license comment.  The tag text is configurable.
@item Generates a doxygen author skeleton.  The tag text is configurable.
@item Generates a doxygen comment skeleton for a C, C++ or Python function or
class, including @code{@@brief}, @code{@@param} (for each named argument), and
@code{@@return}. The tag text as well as a comment block header and footer are
configurable. (Consequently, you can have brief, etc. if you wish, with little
effort.)
@item Ignore code fragment placed in a block defined by #ifdef ... #endif
(C/C++).  The block name must be given to the function.  All of the
corresponding blocks in all the file will be treated and placed in a new block
DOX_SKIP_BLOCK (or any other name that you have configured).  Then you have to
update PREDEFINED value in your doxygen configuration file with correct block
name. You also have to set ENABLE_PREPROCESSING to YES.
@item Generate a doxygen group (beginning and ending). The tag text is
configurable.
@end itemize")
    (home-page "https://www.vim.org/scripts/script.php?script_id=987")
    (license #f)))

(define-public vim-detectindent
  ;; No releases have been tagged.
  (let ((commit "2511f0f02fb046a09fdbdfc8f21c7a6f2d234936")
        (revision "1"))
    (package
      (name "vim-detectindent")
      (version (git-version "0.0.0" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/roryokane/detectindent")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "16k3h64z4ysphchnhgj3jyms51ps0lla885yqznfbknz49pg44cb"))))
      (build-system copy-build-system)
      (arguments
       `(#:install-plan '(("doc" "share/vim/vimfiles/")
                          ("plugin" "share/vim/vimfiles/"))))
      (home-page "https://github.com/roryokane/detectindent")
      (synopsis "Vim script for automatically detecting indent settings")
      (description
       "This script provides a command which will attempt to guess the correct
indent settings for an open file, for use when there is no modeline
available. Specifically, the command sets the following buffer-local
options for you:
@itemize
@item 'expandtab' (tabs vs. spaces)
@item 'shiftwidth' (width of indentation)
@item 'tabstop' (width of a tab character)
@item 'softtabstop' (number of spaces that Tab inserts)
@end itemize

Note that this is a pure Vim implementation, and doesn’t require any external
applications or interpreters.")
      (license #f))))

(define-public vim-deoplete
  (let ((commit "33ed4fa0cd704999f9f74b37640bf6d7334bac37")
        (revision "1"))
    (package
      (name "vim-deoplete")
      (version (git-version "6.1" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/Shougo/deoplete.nvim")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "1gabd83gy3skx3q3prk6drn3dzwag5jmzmp43492mihdak0iks3i"))))
      (build-system copy-build-system)
      (inputs (list python-pynvim))
      (arguments
       `(#:install-plan '(("autoload" "share/vim/vimfiles/")
                          ("doc" "share/vim/vimfiles/")
                          ("plugin" "share/vim/vimfiles/")
                          ("rplugin" "share/vim/vimfiles/"))))
      (home-page "https://github.com/Shougo/deoplete.nvim")
      (synopsis
       "Dark powered asynchronous completion framework for neovim/Vim8")
      (description
       "deoplete is the abbreviation of 'dark powered neo-completion'. It
provides an asynchronous keyword completion system in the current
buffer.")
      (license license:expat))))

(define-public vim-lsp
  (let ((commit "309e9e5c8103bee69eabd152c09eaeec2e0a11f9")
        (revision "1"))
    (package
      (name "vim-lsp")
      (version (git-version "0.1.4" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/prabirshrestha/vim-lsp")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "10hwqd07qipn146iwdih3l7f8nbzz01jh7wpbl9f8l84vi5g5c45"))))
      (build-system copy-build-system)
      (arguments
       `(#:install-plan '(("autoload" "share/vim/vimfiles/")
                          ("doc" "share/vim/vimfiles/")
                          ("ftplugin" "share/vim/vimfiles/")
                          ("plugin" "share/vim/vimfiles/")
                          ("syntax" "share/vim/vimfiles/"))))
      (home-page "https://github.com/prabirshrestha/vim-lsp")
      (synopsis "Async language server protocol plugin for vim and neovim")
      (description "Async language server protocol plugin for vim and neovim")
      ;; vim-lsp itself is licensed under expat however it borrows code from other
      ;; third party projects that are not strictly expat. Setting license to #f for now.
      (license #f))))

(define-public vim-deoplete-vim-lsp
  (let ((commit "af5432f1e063fd4c3a5879aa8c2afe82c17dc1c9")
        (revision "1"))
    (package
      (name "vim-deoplete-vim-lsp")
      ;; No releases have been tagged.
      (version (git-version "0.0.0" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url
                       "https://github.com/lighttiger2505/deoplete-vim-lsp")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "1s6fw6vkpl0yiya22g13v4i14w3n1ds2zr8zdlwpkk44bf0225px"))))
      (build-system copy-build-system)
      (arguments
       `(#:install-plan '(("autoload" "share/vim/vimfiles/")
                          ("plugin" "share/vim/vimfiles/")
                          ("rplugin" "share/vim/vimfiles/"))))
      (home-page "https://github.com/lighttiger2505/deoplete-vim-lsp")
      (synopsis "deoplete source for vim-lsp")
      (description "deoplete source for vim-lsp")
      (license license:expat))))

(define-public vim-bbye
  (let ((commit "25ef93ac5a87526111f43e5110675032dbcacf56")
        (revision "1"))
    (package
      (name "vim-bbye")
      (version (git-version "1.0.1" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/moll/vim-bbye")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "0dlifpbd05fcgndpkgb31ww8p90pwdbizmgkkq00qkmvzm1ik4y4"))))
      (build-system copy-build-system)
      (arguments
       `(#:install-plan '(("plugin" "share/vim/vimfiles/"))))
      (home-page "https://github.com/moll/vim-bbye")
      (synopsis
       "Delete buffers and close files in Vim without closing your windows or messing up your layout. Like Bclose.vim, but rewritten and well maintained.")

      (description
       "Bbye allows you to do delete buffers (close files) without
                   closing your windows or messing up your layout.

Vim by default closes all windows that have the buffer (file) open when you do
:bdelete. If you've just got your splits and columns perfectly tuned, having
them messed up equals a punch in the face and that's no way to tango.

Bbye gives you :Bdelete and :Bwipeout commands that behave like well designed
citizens:
@itemize
@item Close and remove the buffer.
@item Show another file in that window.
@item Show an empty file if you've got no other files open.
@item Do not leave useless [no file] buffers if you decide to edit another file in that window.
@item Work even if a file's open in multiple windows.
@item Work a-okay with various buffer explorers and tabbars.
@end itemize
Regain your throne as king of buffers!")
      ;; Bbye is released under a Lesser GNU Affero General Public License.
      (license license:agpl3))))
