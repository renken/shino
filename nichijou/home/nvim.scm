(define-module (nichijou home nvim)
  #:use-module (gnu home services)
  #:use-module (gnu packages vim)
  #:use-module (gnu services)
  #:use-module (guix gexp)
  #:use-module (ice-9 optargs)
  #:use-module (nichijou packages vim))

(define plugins
  (list vim-airline
        vim-airline-themes
        vim-bbye
        vim-ctrlp
        vim-deoplete
        vim-deoplete-vim-lsp
        vim-detectindent
        vim-doxygen-toolkit
        vim-lsp
        vim-nerdtree
        vim-plantuml-syntax
        vim-syntastic
        vim-tagbar))

(define*-public (get-packages #:key (foreign-distro? #f))
  (if foreign-distro? plugins
      (const neovim plugins)))

(define-public services
  (list (simple-service 'nichijou-nvim-config home-files-service-type
                        `((".config/nvim/init.vim" ,(local-file
                                                     "config/nvim/init.vim"))))))
