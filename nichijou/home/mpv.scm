(define-module (nichijou home mpv)
  #:use-module (gnu home services)
  #:use-module (gnu packages video)
  #:use-module (gnu services)
  #:use-module (guix gexp)
  #:use-module (ice-9 optargs))

(define*-public (get-packages #:key (foreign-distro? #f))
  (if foreign-distro?
      (list)
      (list mpv mpv-mpris)))

;; TODO: Implement mpv home service?
(define-public services
  (list (simple-service 'nichijou-mpv-config home-files-service-type
                        `((".config/mpv/mpv.conf" ,(local-file
                                                    "config/mpv/mpv.conf"))))))
