(define-module (nichijou home zsh)
  #:use-module (gnu home services shells)
  #:use-module (gnu services)
  #:use-module (gnu packages shellutils)
  #:use-module (gnu packages terminals)
  #:use-module (guix gexp)
  #:use-module (ice-9 optargs))

(define packages
  (list zsh-syntax-highlighting zsh-autosuggestions fzf))

(define*-public (get-packages #:key (foreign-distro? #f))
  (if foreign-distro?
      (list) packages))

(define-public services
  (list (service home-zsh-service-type
                 (home-zsh-configuration (xdg-flavor? #t)
                                         (zprofile (list (local-file
                                                          "config/zsh/zprofile")))
                                         (zlogout (list (local-file
                                                         "config/zsh/zlogout")))
                                         (zshrc (list (local-file
                                                       "config/zsh/zshrc")))))))
