set runtimepath^=~/.guix-home/profile/share/vim/vimfiles/

"-------------------------------------------------
" General
"-------------------------------------------------

" Turn off vi compatibility.
set nocompatible

" Set the amount of lines of history to remember.
set history=100

" Set a mapleader key. This is used for extra key combinations.
let mapleader = '\'
map <Space> <leader>

" Better command line completion.
set wildmenu
set wildmode=list:longest,full

" Ignore compiled files.
set wildignore=*.o,*~,*.pyc

" Ignore binary files.
set wildignore^=*.png,*.pdf

" Filetypes
autocmd BufRead,BufNewFile *.irst setfiletype rst
autocmd BufRead,BufNewFile *.puml setfiletype plantuml
autocmd BufRead,BufNewFile *.tikz setfiletype tex
autocmd BufRead,BufNewFile *.tpp setlocal filetype=cpp

" When editing a file, always jump to the last known cursor position.
" Don't do it when the position is invalid, when inside an event handler
" (happens when dropping a file on gvim) and for a commit message (it's
" likely a different one than last time).
autocmd BufReadPost *
  \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
  \ |   exe "normal! g`\""
  \ | endif

" Set the swap directory. The extra '/' suffix is required to make vim create
" unique filenames.
call mkdir(expand("~/.cache/nvim/swap"), "p", 0700)
set dir=~/.cache/nvim/swap//

" Use system clipboard by default
set clipboard +=unnamedplus

"-------------------------------------------------
" User interface
"-------------------------------------------------

" Display line numbers.
set number

" Set the line numbers relative from the current line.
set relativenumber

" Show the (partial) command in status line.
set showcmd

" Always display the status line, even if only one window is displayed.
set laststatus=2

" Shows a dialogue asking if the file has to be saved,
" instead of raising an error.
set confirm

" Keep the cursor in the centre of the buffer if possible.
set scrolloff=10000

" Toggle NERDTree.
map <C-t> :NERDTreeToggle<CR>

" Always show NERDTree on start if no files were specified.
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() ==# 0 && !exists("s:std_in")
  \ | exe 'NERDTreeToggle' | endif

" Disable automatic folding.
set nofoldenable

" Add buffer switching with airline
let g:airline#extensions#tabline#buffer_idx_mode = 1
nmap <leader>1 <Plug>AirlineSelectTab1
nmap <leader>2 <Plug>AirlineSelectTab2
nmap <leader>3 <Plug>AirlineSelectTab3
nmap <leader>4 <Plug>AirlineSelectTab4
nmap <leader>5 <Plug>AirlineSelectTab5
nmap <leader>6 <Plug>AirlineSelectTab6
nmap <leader>7 <Plug>AirlineSelectTab7
nmap <leader>8 <Plug>AirlineSelectTab8
nmap <leader>9 <Plug>AirlineSelectTab9
nmap <leader>bp <Plug>AirlineSelectPrevTab
nmap <leader>bn <Plug>AirlineSelectNextTab
nmap <S-Tab> <Plug>AirlineSelectPrevTab
nmap <Tab> <Plug>AirlineSelectNextTab

" Delete the current buffer without closing the windows
map <leader>bd :Bdelete<CR>
" Close window
map <leader>wd :q<CR>
" Vertical split
map <leader>ws :vsplit<CR>
" Horizontal split
map <leader>wh :split<CR>
" Exit vim
map <leader>qq :qa<CR>

" Invoke completion on : for ledger.
autocmd FileType ledger imap <buffer> : :<C-x><C-o>

" Easy vertical terminal split opening (why is this not built-in?)
cnoreabbrev vterm vert term


"-------------------------------------------------
" Colours and fonts
"-------------------------------------------------

" Enable Doxygen syntax highlighting.
let g:load_doxygen_syntax=1

" Set the color scheme.
" colorscheme wombat256mod

" Set the airline theme and configure the tabline
" let g:airline_theme='wombat'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#fnamecollapse = 1
let g:airline#extensions#tabline#formatter = "default"

" Use 24-bit colours in the terminal, requires 'advanced' terminal emulator.
set termguicolors

" Make the terminal background transparent.
hi Normal guibg=NONE ctermbg=NONE

" Set encoding.
set encoding=utf8

" Set filetype.
set ffs=unix


"-------------------------------------------------
" Search
"-------------------------------------------------

" Highlights all matches of the search pattern.
set hlsearch

" Show all the matches while typing the search command.
set incsearch

" Ignores the case when searching.
set ignorecase

" Case insensitive search, except when capital letters are used.
set smartcase

" Redraw the screen when <C-L> is pressed.
" This also turns off the search highlighting until the next search.
nnoremap <C-L> :nohl<CR><C-L>

" Set the manual page section order for keywordprg
let $MANSECT="3p:3:2:1:n:l:8:0:5:4:9:6:7"

"-------------------------------------------------
" Text (tab, spaces, indent)
"-------------------------------------------------

" Tabsize of 8 (default).
set shiftwidth=8 tabstop=8

" Reset default behaviour to tabs instead of spaces for reStructuredText files.
autocmd Filetype rst setlocal noexpandtab softtabstop=8

" 2 spaces instead of a tab for YAML files.
autocmd Filetype yaml setlocal expandtab shiftwidth=2 softtabstop=2

" 4 spaces instead of a tab for Python and Haskell files.
autocmd Filetype python,haskell,lhaskell,js setlocal expandtab shiftwidth=4
  \ softtabstop=4

" Copy indent from current line when starting a new line.
set autoindent smartindent

" Highlight tabs and trailing spaces
set list listchars=tab:>\ ,trail:·

" Removes trailing spaces when saving
autocmd BufWrite * :call Delete_trailing_spaces()

" Disable softwrapping on long lines
set nowrap

" Enable persistent undo if it is supported
if has('persistent_undo')
  let vundodir = expand('~/.cache/nvim/undo')
  if !isdirectory(vundodir)
    call mkdir(vundodir)
  endif
  let &undodir = vundodir
  set undofile
endif


"-------------------------------------------------
" Deletion
"-------------------------------------------------

" Delete without putting the deleted words into the register.
map <leader>odw "_dw
map <leader>odW "_dW
map <leader>ode "_de
map <leader>odE "_dE
map <leader>odb "_db
map <leader>odB "_dB
map <leader>odd "_dd
map <leader>od^ "_d^
map <leader>od$ "_d$
map <leader>od{ "_d{
map <leader>od} "_d}
map <leader>odi( "_di(
map <leader>odi) "_di)
map <leader>odi' "_di'
map <leader>odi" "_di"
map <leader>odi< "_di<
map <leader>odi> "_di>
map <leader>odf. "_df.
map <leader>odf? "_df?
map <leader>odf! "_df!
map <leader>odt. "_dt.
map <leader>odt? "_dt?
map <leader>odt! "_dt!


"-------------------------------------------------
" Code
" Compiling
"-------------------------------------------------

" Show warnings.
set statusline=%#warningmsg#%{SyntasticStatuslineFlag()}%*

" Set syntastic options.
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_mode_map = {
	\ "mode": "active",
	\ "passive_filetypes": ["python"],
	\}
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0

let g:syntastic_check_on_wq = 0
" Compiles the current file.
map <leader>c :SyntasticCheck<cr>
" Opens the location list that shows the errors.
map <leader>co :lopen<cr><C-w><C-p>
" Closes the location list that shows the errors.
map <leader>cd :lclose<cr>

" Close any preview window that is open
map <leader>cf :pclose<cr>

"" Completion framework.
let g:deoplete#enable_at_startup = 1
" Disable abbreviation of signatures (also causes issues with echodoc)
call deoplete#custom#source('_', 'max_abbr_width', 0)
" Disable auto-completion on backspace character.
call deoplete#custom#option('refresh_backspace', v:false)
" Disable preview window containing function documentation.
set completeopt-=preview
" Map arrow keys to <C-p> and <C-n> for better selection behaviour.
:inoremap <expr><Up> pumvisible() ? "\<C-p>" : "\<Up>"
:inoremap <expr><Down> pumvisible() ? "\<C-n>" : "\<Down>"
" Allow selection of completion with tab and shift+tab.
:inoremap <expr><S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
:inoremap <expr><Tab> pumvisible() ? "\<C-n>" : "\<Tab>"

if executable('pyls')
    " pip install python-language-server
    au User lsp_setup call lsp#register_server({
        \ 'name': 'pyls',
        \ 'cmd': {server_info->['pyls']},
        \ 'allowlist': ['python'],
        \ })
endif

if executable('clangd')
    au User lsp_setup call lsp#register_server({
        \ 'name': 'clangd',
        \ 'cmd': {server_info->['clangd', '-background-index']},
        \ 'whitelist': ['c', 'cpp', 'objc', 'objcpp'],
        \ })
endif

if executable('gopls')
    au User lsp_setup call lsp#register_server({
        \ 'name': 'gopls',
        \ 'cmd': {server_info->['gopls', '-remote=auto']},
        \ 'allowlist': ['go'],
        \ })
endif

function! s:on_lsp_buffer_enabled() abort
    " setlocal omnifunc=lsp#complete
    " Do not use virtual text, they are far too obtrusive.
    " let g:lsp_virtual_text_enabled = 0
    " echo a diagnostic message at cursor position
    " show diagnostic in floating window
    " let g:lsp_diagnostics_float_cursor = 1
    " whether to enable highlight a symbol and its references
    " let g:lsp_highlight_references_enabled = 1
    " let g:lsp_preview_max_width = 80
    setlocal signcolumn=yes
    if exists('+tagfunc') | setlocal tagfunc=lsp#tagfunc | endif
    nmap <buffer> gd <plug>(lsp-definition)
    nmap <buffer> gs <plug>(lsp-document-symbol-search)
    nmap <buffer> gS <plug>(lsp-workspace-symbol-search)
    nmap <buffer> gr <plug>(lsp-references)
    nmap <buffer> gi <plug>(lsp-implementation)
    nmap <buffer> gt <plug>(lsp-type-definition)
    nmap <buffer> <leader>rn <plug>(lsp-rename)
    nmap <buffer> [g <plug>(lsp-previous-diagnostic)
    nmap <buffer> ]g <plug>(lsp-next-diagnostic)
    nmap <buffer> K <plug>(lsp-hover)
    inoremap <buffer> <expr><c-f> lsp#scroll(+4)
    inoremap <buffer> <expr><c-d> lsp#scroll(-4)

    let g:lsp_format_sync_timeout = 1000
    let g:lsp_diagnostics_enabled = 1
    let g:lsp_diagnostics_echo_cursor = 1
    let g:lsp_diagnostics_float_cursor = 0
    let g:lsp_diagnostics_highlights_enabled = 1
    let g:lsp_diagnostics_virtual_text_enabled = 0
    " TODO: Call this when vim-lsp is enabled else call clang-format.
    "autocmd! BufWritePre *.c, *.cpp, *.h, *.hpp, *.rs, *.go call execute('LspDocumentFormatSync')
    " autocmd BufWritePre *.c, *.cpp, *.h, *.hpp, *.rs, *.go LspDocumentFormatSync

    " refer to doc to add more commands
endfunction

augroup lsp_install
    au!
    " call s:on_lsp_buffer_enabled only for languages that has the server registered.
    autocmd User lsp_buffer_enabled call s:on_lsp_buffer_enabled()
augroup END

" Doxygen
let g:DoxygenToolkit_briefTag_pre = ''
let g:DoxygenToolkit_templateParamTag_pre = '\tparam '
let g:DoxygenToolkit_templateParamTag_post = ' '
let g:DoxygenToolkit_paramTag_pre = '\param '
let g:DoxygenToolkit_paramTag_post = ' '
let g:DoxygenToolkit_returnTag = '\return '
let g:DoxygenToolkit_throwTag_pre = '\throw '
let g:DoxygenToolkit_throwTag_post = ' '
let g:DoxygenToolkit_fileTag = '\file '
let g:DoxygenToolkit_authorTag = '\author '
let g:DoxygenToolkit_dateTag = '\date '
let g:DoxygenToolkit_versionTag = '\version '
let g:DoxygenToolkit_blockTag = '\name '
let g:DoxygenToolkit_classTag = '\class '
" C is /** */, C++ is ///
let g:DoxygenToolkit_commentType = 'C'
" allow /** \brief Foo */ for e.g. enum doc.
let g:DoxygenToolkit_compactOneLineDoc = 'yes'
" No empty line between e.g. brief and param section.
let g:DoxygenToolkit_compactDoc = 'yes'

" use <leader>ENTER to generate Doxygen block
map <leader><cr> :Dox<cr>


"-------------------------------------------------
" Code
" Style
"-------------------------------------------------

" Tip: format paragraph with 'gq' in normal mode.
" Line wrap: default to 80 chars, except for python.
" TODO if everyone agrees to move to pyls, remove this bit as it's hard-coded.
set tw=80
autocmd Filetype python setlocal tw=79
" Line wrap: recognise list alignment.
set fo+=n
" Line wrap: remove comment leader when joining lines.
set fo+=j

" Highlight the first 3 characters over 80 character limit.
autocmd BufEnter * highlight OverLength ctermbg=darkgrey guibg=#501010
autocmd BufEnter * match OverLength '\%<84v.\%>81v'

" Set colour for the vertical line that shows the character limit.
highlight ColorColumn ctermbg=Grey guibg=#2d2d2d

" Toggle between the vertical line and the highlighting of characters.
map <leader>cl :call Colorcolumn_highlighting()<cr>

" Toggle between the vertical line and the highlighting of characters.
func! Colorcolumn_highlighting()
  if &l:colorcolumn ==# 81
    setlocal colorcolumn&
    match OverLength '\%<84v.\%>81v'
  else
    setlocal colorcolumn=81
    match OverLength /\%1000v.\+/
  endif
endfunc

" Configure cursor line.
set cursorline

" Disable checks for RST to avoid errors at unknown directives.
let g:syntastic_rst_checkers = []

" Set table mode settings for RST.
let g:table_mode_corner_corner='+'
let g:table_mode_header_fillchar='='


"-------------------------------------------------
" Code
" Shortcuts
"-------------------------------------------------

" Open the quickfix window containing lc diagnostics
map <leader>lco :copen<cr><C-w><C-p>
" Close the quickfix window containing lc diagnostics
map <leader>lcd :cclose<cr>

"-------------------------------------------------
" Code
" Tags
"-------------------------------------------------

map <leader>t :TagbarToggle<cr>


"-------------------------------------------------
" Spell checking
"-------------------------------------------------

" Set spell checking.
set spell spelllang=en_us

" Disable spell check for some problematic filetypes.
autocmd Filetype diff,gitrebase,plantuml,te,yaml setlocal nospell

" Toggle spell checking.
map <leader>ss :setlocal spell!<cr>
map <leader>sl :call Spellcheck_cycle_lang()<cr>

" Shortcuts for spell checking.
map <leader>sn ]s " Next misspelled word.
map <leader>sp [s " Previous mispelled word.
map <leader>sa zg " Add word to dictionary.
map <leader>s? z= " List alternative words.


"-------------------------------------------------
" Functions
"-------------------------------------------------

" Finds and set the dir containing C/C++ compilation database
func! Compilation_database_build_dir_set()
  let l:db_pre = expand('%:p:h')
  let l:db_post = ''
  while !filereadable(l:db_pre . l:db_post . '/compile_commands.json')
    " probe a potential build dir
    if filereadable(l:db_pre . l:db_post . '/build/compile_commands.json')
      let l:db_post = l:db_post . '/build'
      break
    endif
    " otherwise try a directory up
    let l:db_post = l:db_post . '/..'
    " Give up after after 10 dirs up (5 + 3 * 10).
    if strlen(l:db_post) > 35
      let l:db_pre = ''
      let l:db_post = ''
      break
    endif
  endwhile

  " Simplify the dir path, changing /dir/src/../build to /dir/build
  let l:db_dir = simplify(l:db_pre . l:db_post)

  " Stop here if path hasn't changed to avoid language server restart.
  if exists('s:compdb_dir') &&
    \ s:compdb_dir ==# l:db_dir
    return 0
  endif
  let s:compdb_dir = l:db_dir

  " Syntastic.
  " XXX Is this really needed? Remove and experiment.
  " TODO vim-lsp already provides their own database-searching function, find if
  " it can be used to replace this.
  let g:syntastic_c_clang_tidy_args = '-p=''' . s:compdb_dir . ''''
  let g:syntastic_cpp_clang_tidy_args = g:syntastic_c_clang_tidy_args
endfunc

" Removes trailing spaces when saving
" http://amix.dk/vim/vimrc.html
func! Delete_trailing_spaces()
  " Do not execute for diff (patch) files, spaces are part of the context.
  if &filetype ==# 'diff'
    return 0
  endif
  exe "normal mz"
    %s/\s\+$//ge
  exe "normal `z"
endfunc

" Cycle between spellcheck languages
func! Spellcheck_cycle_lang()
  " If spellchecking is disabled, just enable it only
  if &spell ==# 0
    setlocal spell!
    echo 'enabled spell checking'
    return 0
  endif

  if &spelllang ==# 'en_us'
    let l:lang = 'nl'
  else
    let l:lang = 'en_us'
  endif
  let &spelllang = l:lang
  echo 'changed spell checking language to ' . l:lang
endfunc
