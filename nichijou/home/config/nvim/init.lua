function map(mode, shortcut, command)
  vim.api.nvim_set_keymap(mode, shortcut, command, { noremap = true, silent = true })
end

function nmap(shortcut, command)
  map('n', shortcut, command)
end

function imap(shortcut, command)
  map('i', shortcut, command)
end


---------------------------------------------------
-- General
---------------------------------------------------

-- Set the amount of lines of history to remember.
vim.opt.history = 100

-- Set a mapleader key. This is used for extra key combinations.
vim.g.mapleader = "\\"
nmap("<Space>", "<leader>")

-- Better command line completion.
vim.opt.wildmenu = true
vim.opt.wildmode = "list:longest,full"

-- Ignore compiled and binary files.
vim.opt.wildignore = "*.o,*~,*.pyc,*.png,*.pdf"

-- Filetypes
vim.api.nvim_create_autocmd(
    {
        "BufNewFile",
        "BufRead",
    },
    {
        pattern = "*.irst",
        callback = function()
            local buf = vim.api.nvim_get_current_buf()
            vim.api.nvim_buf_vim.opt_option(buf, "filetype", "rst")
        end
    }
)

vim.api.nvim_create_autocmd(
    {
        "BufNewFile",
        "BufRead",
    },
    {
        pattern = "*.puml",
        callback = function()
            local buf = vim.api.nvim_get_current_buf()
            vim.api.nvim_buf_vim.opt_option(buf, "filetype", "plantuml")
        end
    }
)

vim.api.nvim_create_autocmd(
    {
        "BufNewFile",
        "BufRead",
    },
    {
        pattern = "*.tikz",
        callback = function()
            local buf = vim.api.nvim_get_current_buf()
            vim.api.nvim_buf_vim.opt_option(buf, "filetype", "tex")
        end
    }
)

vim.api.nvim_create_autocmd(
    {
        "BufNewFile",
        "BufRead",
    },
    {
        pattern = "*.tpp",
        callback = function()
            local buf = vim.api.nvim_get_current_buf()
            vim.api.nvim_buf_vim.opt_option(buf, "filetype", "cpp")
        end
    }
)

-- TODO: Restore last known cursor position, see https://github.com/neovim/neovim/issues/16339.

-- Set the swap directory to ~/.cache/nvim/swap.
local cache = vim.env.XDG_CACHE_HOME or vim.fn.expand("~/.cache")
local swap = cache .. "/nvim/swap"
if vim.fn.isdirectory(swap) == 0 then
    vim.fn.mkdir(swap, 'p', 0700)
end
-- The extra '/' suffix is required to make vim create unique filenames.
-- XXX: Not sure if this is required in neovim however.
vim.opt.dir = { swap .. "//" }

-- Use system clipboard by default.
vim.opt.clipboard = "unnamedplus"

---------------------------------------------------
-- User interface
---------------------------------------------------

-- Display line numbers.
vim.opt.number = true
vim.opt.relativenumber = true

-- Show the (partial) command in status line.
vim.opt.showcmd = true

-- Always display the status line, even if only one window is displayed.
vim.opt.laststatus = 2

-- Shows a dialogue asking if the file has to be saved, instead of raising an
-- error.
vim.opt.confirm = true

-- Keep the cursor in the centre of the buffer if possible.
vim.opt.scrolloff = 10000

-- Disable automatic folding.
-- vim.opt.nofoldenable = true

-- Delete the current buffer without closing the windows
nmap("<leader>bd", ":Bdelete<CR>")
-- Close window
nmap("<leader>wd", ":q<CR>")
-- Vertical split
nmap("<leader>wv", ":vsplit<CR>")
-- Horizontal split
nmap("<leader>wh", ":split<CR>")
-- Exit vim
nmap("<leader>qq", ":qa<CR>")

-- Easy vertical terminal split opening (why is this not built-in?)
-- cnoreabbrev vterm vert term

-- Set filetype.
vim.opt.ffs = "unix"

vim.opt.termguicolors = true
vim.opt.colorcolumn = "80"

vim.opt.splitbelow = true
vim.opt.splitright = true

---------------------------------------------------
-- Search
--------------------------------------------------

vim.opt.ignorecase = true

vim.opt.smartcase = true

nmap("<C-L>", ":nohl<CR><C-L>")

vim.env.MANSECT = "3p:3:2:1:n:l:8:0:5:4:9:6:7"


---------------------------------------------------
-- Text (tab, spaces, indent)
---------------------------------------------------

vim.bo.softtabstop = 2
vim.opt.autoindent = true
vim.opt.expandtab = true
vim.opt.listchars = { tab = "»-", trail = "·" }
vim.opt.shiftwidth = 2
vim.opt.tabstop = 2
