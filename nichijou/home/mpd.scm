(define-module (nichijou home mpd)
  #:use-module (gnu home services)
  #:use-module (gnu packages mpd)
  #:use-module (gnu services)
  #:use-module (guix gexp)
  #:use-module (ice-9 optargs))

(define*-public (get-packages #:key (foreign-distro? #f))
  (if foreign-distro?
      (list)
      (list mpd)))

;; TODO: Write MPD home service?
(define-public services
  (list (simple-service 'nichijou-mpd-config home-files-service-type
                        `((".config/mpd/mpd.conf" ,(local-file
                                                    "config/mpd/mpd.conf"))))))
