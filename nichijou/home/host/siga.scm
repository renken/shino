(define-module (nichijou home host siga)
  #:use-module (gnu home)
  #:use-module (srfi srfi-1)
  #:use-module ((nichijou home nvim)
                #:prefix nvim:)
  #:use-module ((nichijou home mpd)
                #:prefix mpd:)
  #:use-module ((nichijou home mpv)
                #:prefix mpv:)
  #:use-module ((nichijou home zsh)
                #:prefix zsh:))

;; TODO: Think of a better approach to do this.
;; What if a module's get-packages function expects different parameters?
;; Should modules (packages) be forced to implement the same get-packages
;; signature? If so, how?
(define (apply-get-packages m)
  (m:get-packages #:foreign-distro? #t))

(home-environment
  (packages (fold append
                  (list)
                  (map apply-get-packages
                       (list mpd mpv nvim zsh))))
  (services (append mpd:services mpv:services nvim:services zsh:services)))
