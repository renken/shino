#!/bin/sh

set -eux

cd -- files/etc/wireguard
(umask 077; wg genkey | tee shione.private.key | wg pubkey > shione.public.key)
# TODO: chmod 0600 all files under files/etc/wireguard.
# TODO: Update files/etc/wireguard/wg0.conf accordingly.
cd -- -
