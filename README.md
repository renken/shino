# Nichijou

Experimental Debian packaging and configuration. This project is mainly for
educational purposes. However, the aim is to produce a per-host shared
configuration that would suit my real world usage.

## Host machines

### Siga

[Desktop PC](https://shione.net/2023/06/18/siga-desktop.html) running GNU/Linux
Debian stable.

### Tabi

Lenovo Thinkpad x260 laptop running GNU/Linux Debian stable.

### Shione

[NiPoGi CK10](https://shione.net/2023/12/23/self-hosting-shione.html) Mini PC
server running GNU/Linux Debian stable.

## HOWTO

Because all of my machines run GNU/Linux Debian, I wish to transform
per-package config into `.deb` packages themselves. This project is basically
an application of
[`config-package-dev`](https://debathena.mit.edu/config-package-dev/). This
will allow me to integrate the modifications I introduce to the said packages
within the Debian packaging system which comes with many benefits.

1. Quickly rebuild a host machine from scratch if needed.
2. Know the configuration state of each machine using the package manager.
3. Automatically update configuration by updating the system itself.
4. Integrate complex post-installation configuration, possibly such as
   introducing cron jobs (is it possible?) as part of the configuration
   package.

## Project hierarchy

It is not yet clear to me how I'll do this. I'm thinking perhaps
`config/$host/$package` with the exception of `config/generic/$package` for
host-generic packages such as `neovim` for `siga` and `tabi`.

### NOTES

```console
$ debuild
$ debuild -T clean
```
