# Forgejo container image

It's very basic, possibly insecure but I will improve that later on. The key
points to remember is that all secrets *must* be provided in the form of files
to avoid any leakage.

## Create the admin user

Unfortunately there is no way to pre-seed forgejo's DB with an admin user to
avoid any post-installation procedures. The following allows you to create it.

```console
$ podman exec \
    "$container_id" \
    /usr/local/bin/forgejo \
        -w /var/lib/forgejo \
        -c /etc/forgejo/app.ini \
        admin user create \
            --admin \
            --username "$username" \
            --password "$basic_password" \
            --email "$email"
```

**NOTE:**: You *should* do this before exposing your container to the internet
just to be safe. Make sure to change the password once the account has been
created.

## TODO

* Switch to PostgreSQL once I have an image emulating shione's setup running.
* Test forgejo's OAuth2 thouroughly since it will be used to authenticate other
  services running on shione.
* Switch to redis for caching once an image of it is available.
* Host-container SSH forwarding.
* systemd service.
    * Debian packaging which also creates a `git` UNIX user and stuff?
    * Maybe just use ansible?
* Figure out what to backup.
    * Everything will be backed up using borgbase on the host side, possibly
      backup git repositories, databases and anything oauth2-related?
* Setup mail.
* Is `PASSWORD_HASH_ALGO = pbkdf2_hi` the best we could use?
* Disable as many unneeded services integrated by default as possible? Is it
  possible to strip the binary from such services e.g., ACME-related code?
* What to do with CI/CD? It would be nice to deploy shione services using
  forgejo.
* Packaging forgejo using Guix or Debian? Is this too much?
* Separate the base image from the image responsible only for copying
  host-specific secret artifacts to the final image.
