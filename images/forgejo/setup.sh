#!/bin/sh

set -eux

# Secure forgejo files before anything.
adduser \
  --system \
  --shell /bin/bash \
  --gecos 'Git Version Control' \
  --group \
  --disabled-password \
  --home /home/git git

mkdir -p /var/lib/forgejo
chown git:git /var/lib/forgejo 
chmod 750 /var/lib/forgejo

mkdir -p /etc/forgejo
chown -R root:git /etc/forgejo

for file in app.ini lfs_jwt_secret secret_key internal_token oauth2_jwt_secret; do
  chmod 0640 /etc/forgejo/"$file"
done

apt-get update -y

apt-get upgrade -y

apt-get --no-install-recommends install -y \
  ca-certificates \
  dirmngr \
  gpg \
  gpg-agent \
  curl \
  git \
  git-lfs \
  systemd

version=7.0.3
curl -LO \
  "https://codeberg.org/forgejo/forgejo/releases/download/v$version/forgejo-$version-linux-amd64"
gpg --keyserver keys.openpgp.org --recv EB114F5E6C0DC2BCDD183550A4B61A2DC5923710
curl -LO \
  "https://codeberg.org/forgejo/forgejo/releases/download/v$version/forgejo-$version-linux-amd64.asc"
gpg --verify forgejo-$version-linux-amd64.asc forgejo-$version-linux-amd64


chmod +x "forgejo-$version-linux-amd64"
mv "forgejo-$version-linux-amd64" /usr/local/bin/forgejo
